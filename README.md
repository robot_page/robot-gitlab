Instruçoes Banco e Serviços do Banco

-- Pre Requisitos:
        - Ter instalado:
                * Makefile
                * Pyhton3
                * Docker
                * Docker-compose

1 - Excute:
        make robot

2 - Verifique se os ips do Banco e Serviço estao correto:
        docker ps
        docker inspect "ID Container" | grep IP

        ip_db = 172.18.0.2
        ip_service_db = 172.18.0.3

        # Caso for diferente atualizar:
                constants.py - POSTGRES_URL=getenv('POSTGRES_URL','172.18.0.2')

                conexao-GitLab/constants.py - URL_SERVICE_DB_USERS = getenv('URL_SERVICE_DB_USERS','http://172.18.0.3:9000/users/')
                                        URL_SERVICE_DB_COMMITS = getenv('URL_SERVICE_DB_COMMITS','http://172.18.0.3:9000/commits/')
                                        URL_SERVICE_DB_COMMITS_USER = getenv('URL_SERVICE_DB_COMMITS_USER','http://172.18.0.3:9000/commits-user/')
                                        URL_SERVICE_DB_COMMITS_LAST = getenv('URL_SERVICE_DB_COMMITS_LAST','http://172.18.0.3:9000/last-commit/')

                apos atualizar execute passo 1.

3 - Excute o projeto conexao-GitLab:
        - ver README.md do projeto

