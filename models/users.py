from db import db
from sqlalchemy_utils import database_exists,create_database, drop_database
from sqlalchemy.schema import DropTable
from sqlalchemy import *

class Users(db.Model):
    __tablename__ = 'user'

    id_user = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String,nullable=False)
    commits_id = db.relationship("Commits", backref="user")

    def __init__(self, id_user, user):
        self.id_user = id_user
        self.user = user
        
    def save_to_db_user(self):
        db.session.add(self)
        db.session.commit()

    def save_to_bd_bulk(self):
        db.session.bulk_save_objects(self)
        db.session.commit()

    def jsonUser(self):
        return dict(id_user=self.id_user,user=self.user)

    @classmethod
    def find_by_name(cls, _name):
        return cls.query.filter_by(user=_name).first()
    
    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id_user=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.filter_by().all()

    @classmethod
    def drop_table_user(cls):
        db.drop_all()
        db.create_all()
    


    

    

