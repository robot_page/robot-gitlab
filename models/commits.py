from db import db
import sqlalchemy

class Commits(db.Model):
    __tablename__ = 'commit'
    
    id_commit = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.String, nullable=False)
    hashcommit = db.Column(db.String, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id_user'))

    def __init__(self, id_commit, date, hashcommit,user_id):
        self.id_commit = id_commit
        self.date = date
        self.hashcommit = hashcommit
        self.user_id = user_id     

    def save_to_db_commit(self):
        db.session.add(self)
        db.session.commit()

    def jsonCommit(self):
        return dict(id_commit=self.id_commit, date=self.date, hashcommit=self.hashcommit, user_id=self.user_id)

    @classmethod
    def find_by_user_id(cls,_userId):
        return cls.query.filter_by(user_id=_userId).all()

    @classmethod
    def find_by_id_commit(cls,_idCommit):
        return cls.query.filter_by(id_commit=_idCommit).first()
    
    @classmethod
    def find_all(cls):
        return cls.query.filter_by().all()

    @classmethod
    def find_last_commits_user(cls,_userId):
        return cls.query.filter_by(user_id=_userId).order_by(sqlalchemy.asc(Commits.id_commit)).first()
