from os import getenv

POSTGRES_USER = getenv('POSTGRES_USER', 'postgres')
POSTGRES_PW = getenv('POSTGRES_PW', '123')
POSTGRES_URL = getenv('POSTGRES_URL', '172.18.0.2')
POSTGRES_DB = getenv('POSTGRES_DB', 'db_robot')
