from flask_restful import Resource,reqparse, ResponseBase,abort,inputs
from models.commits import Commits
import json

class CommitsResource(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('id_commit', type=int, required=False)
            parser.add_argument('date',type=str, required=True)
            parser.add_argument('hashcommit',type=str,required=True)
            parser.add_argument('user_id',type=int,required=False)

            data = parser.parse_args()
            commit = Commits(**data)
        except Exception as e:
            abort(400)

        commit.save_to_db_commit()

        return commit.jsonCommit(),200

    def get(self, _idCommit=None):
        if _idCommit:
            
            commit = Commits.find_by_id_commit(_idCommit)

            if not commit:
                return ResponseBase(status=404)
            return commit.jsonCommit(),200

        commit = Commits.find_all()

        if not commit:
            return ResponseBase(status=404)
        return [commit.jsonCommit() for commit in commit],200
        

class CommitsUserResource(Resource):
    def get(self, _userId=None):
        if _userId:
            
            commit = Commits.find_by_user_id(_userId)

            if not commit:
                return ResponseBase(status=404)
            return [commit.jsonCommit() for commit in commit],200

class LastCommitUser(Resource):
    def get(self, _userId=None):
        if _userId:
            last_commit = Commits.find_last_commits_user(_userId)

            if not last_commit:
                return ResponseBase(status=404)
            return last_commit.jsonCommit(),200
