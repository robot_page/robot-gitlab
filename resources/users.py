from flask_restful import Resource,reqparse, ResponseBase,abort,inputs,request
from models.users import Users
from errors.error import ErrorPostDB
import json
from db import db

class UsersResource(Resource):
    def get(self,_id=None,_name=None):
        if _id:
            user = Users.find_by_id(_id)

            if not user:
                return ResponseBase(status=404)
            return user.jsonUser(),200
        if _name:
            user = Users.find_by_name(_name)

            if not user:
                return ResponseBase(status=404)
            return user.jsonUser(),200
        
        user = Users.find_all()
        if not user:
            return ResponseBase(status=404)
        return [user.jsonUser() for user in user],200

    def post(self):
        try:
            message = None
            data_request = request.get_json()

            data_users = []
            if type(data_request) == list:
                for data_convert in data_request:
                    data_dumps = json.dumps(data_convert)
                    data_loads = json.loads(data_dumps)

                    data = Users(**data_loads)
                    data_users.append(data)

                users = []
                for user in data_users:
                    users.append(user)

            else:

                parser = reqparse.RequestParser()
                parser.add_argument('id_user', type=int, required=False)
                parser.add_argument('user', type=str, required=True)
                data = parser.parse_args()
                
                user = Users(**data)
        
        except ValueError as e:
            message = "Existe mais de um valor para salvar. " + str(e)
        except ErrorPostDB as e:
            message = "Algo deu errado no Post. " + str(e)
        finally:
            if message:
                message = message
                retcode = 400
                return message,retcode
            else:
                message = None
                retcode = 200

        if type(data_request) == list:
            Users.save_to_bd_bulk(users)
        
        else:
            user.save_to_db_user()


class DropTable(Resource):
    def delete(self):
        drop_user = Users.drop_table_user()