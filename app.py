import psycopg2
from flask import Flask
from flask_restful import Api
from constants import POSTGRES_USER,POSTGRES_PW,POSTGRES_DB,POSTGRES_URL

from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import database_exists,create_database, drop_database

from db import db

from resources.users import UsersResource,DropTable
from resources.commits import CommitsResource,CommitsUserResource,LastCommitUser

app = Flask(__name__)
cors = CORS(app)

DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}'.format(user=POSTGRES_USER,pw=POSTGRES_PW,url=POSTGRES_URL,
        db=POSTGRES_DB)

app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

api = Api(app)
api.add_resource(UsersResource,'/users/','/users/<int:_id>','/users/<string:_name>',endpoint='users')
api.add_resource(CommitsResource,'/commits/','/commits/<int:_idCommit>', endpoint='commits')
api.add_resource(CommitsUserResource,'/commits-user/','/commits-user/<int:_userId>',endpoint='commits-user')
api.add_resource(LastCommitUser,'/last-commit/<int:_userId>',endpoint='last-commit')
api.add_resource(DropTable,'/drop-table/',endpoint='drop-table')


if not database_exists(DB_URL):
    create_database(DB_URL)
if database_exists(DB_URL):
    drop_database(DB_URL)
    create_database(DB_URL)


@app.before_first_request
def create_table():
    db.drop_all()
    db.create_all()

if __name__ == "__main__":
    db.init_app(app)
    app.run(debug=False,host='0.0.0.0',port=9000)
