FROM python:3.6-alpine

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

COPY . ./

EXPOSE 9000

CMD ["python","app.py"]